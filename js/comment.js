var params = (new URL(document.location)).searchParams;
var id = params.get("id");

var editCommitId = 0;
var editAvatar = '';
var property = '';
let comments = [];
var avatar;


function getNewById() {
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && (this.status === 200 || this.status === 201)) {
                val = JSON.parse(this.responseText);
                document.getElementById("title").innerHTML = "for "+ val.title;
                console.log('response BY ID :: ', val);
            }
            if (this.readyState === 4 && this.status === 400) {
                alert(this.responseText);
            }
            if (this.readyState === 4 && this.status === 500) {
                alert(this.responseText);
            }
        };
        xhttp.open("GET", "https://5d2c2f2b8c90070014972225.mockapi.io/api/v2/news/"+id, true);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send();
}


function loadComment() {
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            comments = JSON.parse(this.responseText);
            loadView();
        }
    };
    xhttp.open("GET", "https://5d2c2f2b8c90070014972225.mockapi.io/api/v2/news/" + id + "/comments", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send();
}

function loadView() {
    comments.forEach((comment, i) => {

        var commentBody = document.createElement("div");
        commentBody.innerHTML = `
                                            <div class="user-comment-container">
                                            <div class="dp">
                                                <img src="` + comment["avatar"] + `" alt="image"/>
                    </div>
                    <div class="user-comment">
                    <div class="time"><span class="sender">` + comment["name"] + `</span> commented at ` + getFormattedDate(comment["createdAt"]) + `
                    </div>
                <div class="comment">` + comment["comment"] + `</div>
                <div class="btn-group">
                <button class="edit-btn" onclick="editComment(` + comment['id'] + `,this)">edit</button> 
                <button class="delete-btn" onclick="deleteComment(` + comment['id'] + `,this)">delete</button></div>
                </div>
                <div style="clear: both"></div>
                    </div>`;
        document.getElementById("comment-body").appendChild(commentBody)

    })
}
function loadImage() {
    let xhttp = new XMLHttpRequest();
    let val;
    let images = [];
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            val = JSON.parse(this.responseText);
            console.log("val image:: ", val);
            val.forEach(val => {
                images.push(val.avatar);
            });
            let i = 0;
            var img = document.createElement("img");
            // document.getElementById("img").innerHTML = "";

            setInterval(function () {
                img.setAttribute("src", images[i]);
                img.setAttribute('class', 'image')
                document.getElementById("img").appendChild(img);
                i += 1;
                if (i === images.length) {
                    i = 0;
                }
            }, 3000)
        }
    };
    xhttp.open("GET", "https://5d2c2f2b8c90070014972225.mockapi.io/api/v2/news/" + id + "/images", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send();
}

function getFormattedDate(date) {
    var dt = new Date(date);
    return dt.getFullYear() + "/" + (parseInt(dt.getMonth()) + 1) + "/" + dt.getDate() + "  " + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
}

function edit(index, data) {
    const id = index.parentElement.parentElement.cells[0].innerHTML;

    window.location.href = 'detail.html?id=' + id;

}

function onSelectImage(file) {
    var input = file.target;
    var reader = new FileReader();
    reader.onload = function () {
        var dataURL = reader.result;
        var output = document.getElementById('output');
        output.src = dataURL;
    };
    avatar = input.files[0];
    reader.readAsDataURL(input.files[0]);
}


function submitComment() {
    var name = document.getElementById('name').value;
    var comment = document.getElementById('comment').value;
    var check = validateForm(name, comment);
    if(check) {
        var formData = new FormData();
        formData.append("name", name);
        formData.append("comment", comment);
        formData.append("avatar", avatar);
        formData.append("newsId", id);

        let xhttp = new XMLHttpRequest();
        let val;
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                val = JSON.parse(this.responseText);
                comments.push(val);
                responseMessage("Comment was added successfully");
            }
            if (this.readyState === 4 && this.status === 400) {
                alert(this.responseText);
            }
            if (this.readyState === 4 && this.status === 500) {
                alert(this.responseText);
            }
        };
        xhttp.open("POST", "https://5d2c2f2b8c90070014972225.mockapi.io/api/v2/news/" + id + "/comments", true);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send(JSON.stringify(formData));
    }
}

function editComment(commentId, prop) {
    editCommitId = commentId;
    property = prop;
    var elem = prop.parentElement.parentElement.parentElement;
    editAvatar = elem.getElementsByClassName("dp")[0].getElementsByTagName("img")[0].getAttributeNode("src").value;
    var name = elem.getElementsByClassName("sender")[0].innerHTML
    var comment = elem.getElementsByClassName("comment")[0].innerHTML

    document.getElementById("name").value = name;
    document.getElementById("comment").value = comment;
    var output = document.getElementById('output');
    output.src = editAvatar;

    document.getElementById("submit-comment").style.display = "none";
    document.getElementById("edit-comment").style.display = "block";
    document.getElementById("add-your-comment").style.display = "none";
    document.getElementById("edit-your-comment").style.display = "block";
    document.getElementById("img-upload").style.display = "none";
}

function resetCommentForm() {
    document.getElementById("name").value = "";
    document.getElementById("comment").value = "";

    var output = document.getElementById('output');
    output.src = {};

    document.getElementById("submit-comment").style.display = "block";
    document.getElementById("edit-comment").style.display = "none";
    document.getElementById("add-your-comment").style.display = "block";
    document.getElementById("edit-your-comment").style.display = "none";
    document.getElementById("img-upload").style.display = "block";
}

function edit() {
    let xhttp = new XMLHttpRequest();
    var elem = property.parentElement.parentElement.parentElement;
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            var response = JSON.parse(this.responseText);

            elem.getElementsByClassName("dp")[0].getElementsByTagName("img")[0].getAttributeNode("src").value = response['avatar'];
            elem.getElementsByClassName("sender")[0].innerHTML = response['name']
            comment = elem.getElementsByClassName("comment")[0].innerHTML = response['comment'];

            responseMessage("Comment was updated successfully");
            resetCommentForm();
        }
        if (this.readyState === 4 && this.status === 400) {
            alert(this.responseText);
        }
        if (this.readyState === 4 && this.status === 500) {
            alert(this.responseText);
        }
    };

    var name = document.getElementById('name').value;
    var comment = document.getElementById('comment').value;

   const check = validateForm(name, comment);

   if(check) {
       const payLoad = {
           "name": name,
           "comment": comment,
           "newsId": id,
           "avatar": editAvatar
       };

       xhttp.open("PUT", "https://5d2c2f2b8c90070014972225.mockapi.io/api/v2/news/" + id + "/comments/" + editCommitId, true);
       xhttp.setRequestHeader("Content-type", "application/json");
       xhttp.send(JSON.stringify(payLoad));
   }
}

function deleteComment(commentId, prop) {
    var r = confirm("You are about to delete a comment. Click OK to continue");
    if (r == false) {
        return;
    }
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            var elem = prop.parentElement.parentElement.parentElement;
            elem.parentNode.removeChild(elem);
        }
        if (this.readyState === 4 && this.status === 400) {
            alert(this.responseText);
        }
        if (this.readyState === 4 && this.status === 500) {
            alert(this.responseText);
        }
    };
    xhttp.open("DELETE", "https://5d2c2f2b8c90070014972225.mockapi.io/api/v2/news/" + id + "/comments/" + commentId, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send();
}


function validateForm(name, comment) {
    if(!name || name === "" ) {
        alert("Title is required");
        return false;
    }else if(!comment || comment === "" ) {
        alert("Url is required");
        return false;
    }else {
        return true
    }
}

function responseMessage(message) {
    document.getElementById("response").innerHTML = message;
    setTimeout(function () {
        document.getElementById("response").innerHTML = "";
    }, 5000);
}

function backToNews() {
    window.location.href = 'index.html';
}