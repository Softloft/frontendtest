let xhttp = new XMLHttpRequest();
let val;
let i = 0;
let page = 1;
let limit = 2;

var editNewsId = 0;
var editAvatar = '';
var property = '';
var news = [];
var avatar;

function initiate(page, limit) {
    if (page < 1) {
        return;
    }

    if(page<= 1) {
        document.getElementById('backNav').style.visibility = "hidden";
    }else {
        document.getElementById('backNav').style.visibility = "visible";
    }

    document.getElementById('body').innerHTML = " ";
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            news = JSON.parse(this.responseText);
            loadView();
        }
    };
    xhttp.open("GET", paginatedUrl(page, limit), true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send();
}

function loadView() {
    news.forEach((newInfo) => {
        var container = document.createElement("div");
        container.innerHTML = `
                                   <div class="container">
                                    <div class="image">
                                        <img id="dp" src="` + newInfo['avatar'] + `" style="border-radius: 50%; border: 5px solid #ffffff">
                                    </div>
                                    <div class="description">
                                         <div class="title"><span>Title:</span> <span>` + newInfo['title'] + `</span></div>
                                         <div class="url"><span>Url:</span> <span><a href="` + newInfo['url'] + `" target="_blank">` + newInfo['url'] + `</a></span></div>
                                         <div class="date"><span>Date</span> <span>` + getFormattedDate(newInfo['createdAt']) + `</span></div>
                                         <button class="edit-btn" onclick="editNews(` + newInfo['id'] + `,this)">edit</button> 
                                         <button class="edit-btn" onclick="newPage(` + newInfo['id'] + `)">View Comment</button> 
                                    </div>
                                    <div style="clear: both"></div>
                                </div>`;
        document.getElementById('body').appendChild(container);
    });
}
function paginatedUrl(page, limit) {
    return `https://5d2c2f2b8c90070014972225.mockapi.io/api/v2/news?page=` + page + `&limit=` + limit;
}

function getFormattedDate(date) {
    var dt = new Date(date);
    return dt.getFullYear() + "/" + (parseInt(dt.getMonth()) + 1) + "/" + dt.getDate() + "  " + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
}

function edit(index, data) {
    const id = index.parentElement.parentElement.cells[0].innerHTML;

    window.location.href = 'detail.html?id=' + id;

}

function newPage(id) {
    window.location.href = 'comment.html?id=' + id;
}


function submitNews() {
    var author = document.getElementById('author').value;
    var title = document.getElementById('title').value;
    var url = document.getElementById('url').value;

    const check = validateForm(author, title, url);

    if(check) {
        var formData = new FormData();
        formData.append("title", title);
        formData.append("url", url);
        formData.append("avatar", avatar);

        let xhttp = new XMLHttpRequest();
        let val;
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && (this.status === 200 || this.status === 201)) {
                val = JSON.parse(this.responseText);
                responseMessage("News was added successfully");
                resetCommentForm()
            }
            if (this.readyState === 4 && this.status === 400) {
                alert(this.responseText);
            }
            if (this.readyState === 4 && this.status === 500) {
                alert(this.responseText);
            }
        };
        xhttp.open("POST", "https://5d2c2f2b8c90070014972225.mockapi.io/api/v2/news", true);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send(JSON.stringify(formData));
    }
}

function editNews(newsId, prop) {
    editNewsId = newsId;
    property = prop;
    var elem = prop.parentElement.parentElement.parentElement;

    editAvatar = elem.getElementsByClassName("image")[0].getElementsByTagName("img")[0].getAttributeNode("src").value;
    var title = elem.getElementsByClassName("title")[0].getElementsByTagName("span")[1].innerHTML;
    var url = elem.getElementsByClassName("url")[0].getElementsByTagName("a")[0].text

    document.getElementById("author").value = "";
    document.getElementById("title").value = title;
    document.getElementById("url").value = url;
    var output = document.getElementById('output');
    output.src = editAvatar;

    document.getElementById("add-news").style.display = "none";
    document.getElementById("edit-news").style.display = "block";
    document.getElementById("submit").style.display = "none";
    document.getElementById("edit").style.display = "block";
}

function resetCommentForm() {
    document.getElementById("author").value = "";
    document.getElementById("title").value = "";
    document.getElementById("url").value = "";
    var output = document.getElementById('output');
    output.src = {};

    document.getElementById("add-news").style.display = "block";
    document.getElementById("edit-news").style.display = "none";
    document.getElementById("submit").style.display = "block";
    document.getElementById("edit").style.display = "none";
}

function edit() {
    let xhttp = new XMLHttpRequest();
    var elem = property.parentElement.parentElement.parentElement;
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && (this.status === 200 || this.status === 201)) {
            var response = JSON.parse(this.responseText);

            elem.getElementsByClassName("image")[0].getElementsByTagName("img")[0].getAttributeNode("src").value = response['avatar'];
            elem.getElementsByClassName("title")[0].getElementsByTagName("span")[1].innerHTML = response['title'];
            elem.getElementsByClassName("url")[0].getElementsByTagName("a")[0].innerHTML = response['url'];

            responseMessage("News was updated successfully");

            resetCommentForm();
        }
        if (this.readyState === 4 && this.status === 400) {
            alert(this.responseText);
        }
        if (this.readyState === 4 && this.status === 500) {
            alert(this.responseText);
        }
    };

    var author = document.getElementById('author').value;
    var title = document.getElementById('title').value;
    var url = document.getElementById('url').value;

    const check = validateForm(author, title, url);

    if(check) {
        const payLoad = {
            "author": author,
            "title": title,
            "url": url,
            "id": editNewsId,
            "avatar": editAvatar
        };

        xhttp.open("PUT", "https://5d2c2f2b8c90070014972225.mockapi.io/api/v2/news/" + editNewsId, true);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send(JSON.stringify(payLoad));
    }
}

function validateForm(author,title, url) {
    if(!title || title === "" ) {
        alert("Title is required");
        return false;
    }else if(!url || url === "" ) {
        alert("Url is required");
        return false;
    }else {
        return true
    }
}

function responseMessage(message) {
    document.getElementById("response").innerHTML = message;
    setTimeout(function () {
        document.getElementById("response").innerHTML = "";
    }, 5000);
}

function onSelectImage(file) {
    var input = file.target;
    var reader = new FileReader();
    reader.onload = function () {
        var dataURL = reader.result;
        var output = document.getElementById('output');
        output.src = dataURL;
    };
    avatar = input.files[0];
    reader.readAsDataURL(input.files[0]);
}
